/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.sysloc.dao;

import javax.persistence.EntityManager;
import java.util.List;

/**
 *
 * @author sala304b
 */
public abstract class DAO<T> {
    protected EntityManager em;
    
    private final Class<T> entidade;
    
  public DAO(Class<T> entidade) {
      this.entidade = entidade;
  }
    
  public void save(T objeto) {
      this.em = JPAUtil.getEntityManager();
      em.getTransaction().begin();
      em.persist(objeto);
      em.getTransaction().commit();
      em.close();
  }
}