/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.sysloc.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author sala304b
 */
public class Diretor {
    
  @Id
  @GeneratedValue (strategy = GenerationType.AUTO)
  @Column (name = "id")
  private int codigo;
  
  @Column ( name = "Primeiro_Nome" , nullable = false , length = 200)
  private String nome;
  
  @Column ( name = "Ultimo_Nome" , nullable = false , length = 200)
  private String sobreNome;
 
  @Column ( name = "Local_Nascimento" , nullable = false , length = 200)
  private String localNascimento;
  
  @Temporal(TemporalType.DATE)
  @Column ( name = "Aniversario" , nullable = false)
  private Date dataAniversario;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobreNome() {
        return sobreNome;
    }

    public void setSobreNome(String sobreNome) {
        this.sobreNome = sobreNome;
    }

    public String getLocalNascimento() {
        return localNascimento;
    }

    public void setLocalNascimento(String localNascimento) {
        this.localNascimento = localNascimento;
    }

    public Date getDataAniversario() {
        return dataAniversario;
    }

    public void setDataAniversario(Date dataAniversario) {
        this.dataAniversario = dataAniversario;
    }
  
  
   
  
}
