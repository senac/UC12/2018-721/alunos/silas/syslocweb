/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.sysloc.model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author sala304b
 */
public class Filme {
    


@Id
  @GeneratedValue (strategy = GenerationType.AUTO)
  @Column (name = "id")
  private int codigo;
  
  @Column ( name = "Titulo" , nullable = false , length = 200)
  private String titulo;
  
  
  @Column ( name = "Ano Lancamento" , nullable = false)
  private int anoLancamento;
  
   @Column ( name = "Avaliacao" , nullable = false)
  private int avaliacao;
   
    @Column ( name = "Tempo Filme" , nullable = false)
  private int tempoFilme;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getAnoLancamento() {
        return anoLancamento;
    }

    public void setAnoLancamento(int anoLancamento) {
        this.anoLancamento = anoLancamento;
    }

    public int getAvaliacao() {
        return avaliacao;
    }

    public void setAvaliacao(int avaliacao) {
        this.avaliacao = avaliacao;
    }

    public int getTempoFilme() {
        return tempoFilme;
    }

    public void setTempoFilme(int tempoFilme) {
        this.tempoFilme = tempoFilme;
    }

 
    
}